import { useEffect, useState } from "react";
import Modal from "../modal/modal";
import * as S from "./style";

interface IPanel {
  title: string;
  paymentsData: Payment[];
}
// TODO: corrigir o código para que seja removido os pagamentos do context
export default function Panel({ title, paymentsData }: IPanel) {
  const [isOpen, setOpen] = useState(false); 
  const [panelData, setPanelData] = useState<Payment>();
  const [ newListPayments, setNewListPayments] = useState<any>();

  useEffect(()=>{
    if(!newListPayments){
      setNewListPayments(paymentsData)
    }
  },[]) 

  const openModal = (item:any) => {
    setPanelData(item)
    setOpen(true)
  }
  const handleDelete = () => {
    setOpen(false)
   const novaLista = newListPayments && newListPayments.reduce((acc:any,obj:any) => {
        if(obj.id !== panelData.id){
          acc.push(obj)
        }
        return acc
      },[])

    setNewListPayments(novaLista)   
  }

  return (
    <>
      <S.Panel>
        <S.Title>{title}</S.Title>
        {newListPayments &&
          newListPayments.map((item: any, index: number) => (
            <S.Text key={index} onClick={()=> openModal(item)}>
              <p>{item.id}</p>
              <p>{item.type}</p>
              <p>{item.value}</p>
            </S.Text>
          ))}
      </S.Panel>
      {isOpen && <Modal isOpen={isOpen} onClose={handleDelete} modalData={panelData}/>}
    </>
  );
}
