import { styled } from "styled-components";

export const Panel = styled.div`
    display:flex;
    width: 100%;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    gap: 4px;
`

export const Title = styled.h2`
    color: white;
    text-align: center;
    padding: 0;
    margin: 0;
    width: 100%;
`

export const Text = styled.div`
    width: 100%;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    text-align: center;
    background-color: white;
    border-radius: 4px;
    color: gray;
    cursor: pointer;
`