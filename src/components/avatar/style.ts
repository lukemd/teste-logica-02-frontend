import styled  from "styled-components"

export const Avatar = styled.img`
    height: 80px;
    width: 80px;
    border-radius: 50%;
`