import { useContext } from "react";
import Panel from "../../components/panel/panel";
import * as S from "./style";
import UserInfoContexto from "../../context/context";
import { useNavigate } from "react-router-dom";

export default function UserInfo() {
  const { usersData, selectedUserId } = useContext(UserInfoContexto);
  const navigate = useNavigate();

  return (
    <>
      <button onClick={() => navigate("/")}>voltar</button>

      <S.Grid>
        {usersData[selectedUserId] &&
          usersData[selectedUserId].payments.map((grid: any, index: number) => (
            <Panel title={"Grid " + (index + 1)} paymentsData={grid} key={index} />
          ))}
      </S.Grid>
    </>
  );
}
