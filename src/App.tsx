import { Route, Routes } from 'react-router-dom'
import './App.css'
import Login from './pages/login/login'
import UserInfo from './pages/user-info/user-info'

function App() {
  
  return (
      <Routes>
        <Route path="/" element={<Login/>}/>
        <Route path="/userinfo" element={<UserInfo/>}/>
      </Routes>

  )
}

export default App