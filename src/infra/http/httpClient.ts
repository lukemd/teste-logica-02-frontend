import axios, { AxiosResponse } from 'axios';

export type HttpClientDefaultResponse = AxiosResponse;

const API_URL = import.meta.env.VITE_APP_BASE_URL

export const httpClient = axios.create({
  baseURL: API_URL
});