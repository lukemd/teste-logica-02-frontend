import * as S from "./style";

interface IAvatar {
    urlImage: string
}

export default function Avatar({urlImage}:IAvatar) {
  return (
    <>
      <S.Avatar
        src={urlImage}
      />
    </>
  );
}
