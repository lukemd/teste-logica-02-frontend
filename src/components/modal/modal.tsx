import * as S from "./style";

type ModalProps = {
  isOpen: boolean;
  onClose: () => void;
  modalData: any;
};

export default function Modal({ isOpen, onClose, modalData }: ModalProps) {
  return (
    <S.Overlay open={isOpen}>
      <S.ModalContent>
        <div>
            <button onClick={onClose}>Delete</button>
        </div>
      </S.ModalContent>
    </S.Overlay>
  );
}
