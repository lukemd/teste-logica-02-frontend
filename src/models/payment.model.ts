interface Payment {
    id: number;
    type: string;
    value: number;
  }