export interface User {
    id: number;
    name: string;
    payments: Payment[];
  }