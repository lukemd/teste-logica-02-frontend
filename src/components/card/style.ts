import styled  from "styled-components"

export const Card = styled.div`
    display:flex;
    justify-content: flex-start;
    align-items: center;
    background-color: white;
    border-radius: 4px;
    color: gray;
    padding: 24px;
    gap: 16px;
    cursor: pointer;
`

export const Title = styled.h2`
    text-align: start;
    padding: 0;
    margin: 0;
`

export const Subtitle = styled.p`
    padding: 0;
    margin: 0;
`

export const CardInfo = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    gap: 4px;
`