import * as S from "./style";
import { useNavigate } from 'react-router-dom'
import UserInfoContexto from "../../context/context";
import { useContext} from 'react';
import { User } from "../../models/user.model";

export default function Card({id, name }: User) {

  const { getPaymentByUser } = useContext(UserInfoContexto);
  const navigate = useNavigate()

  const getInfoHandle = async () => {    
    getPaymentByUser(id)   
    navigate("userinfo")
  }
  return (
    <S.Card onClick={()=> getInfoHandle()}>
      <S.CardInfo>
        <S.Title>{name}</S.Title>
      </S.CardInfo>
    </S.Card>
  );
}
