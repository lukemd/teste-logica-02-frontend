import styled from 'styled-components'

type ModalProps = {
    open: boolean;
}

export const Overlay = styled.div<ModalProps>`
    display: ${prop => prop.open ? 'flex' : 'none' };
    justify-content:center;
    align-items:center;
    background-color: rgba(0,0,0,.5);
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;    
`

export const ModalContent = styled.div`
    background-color: white;
    border-radius: 4px;
    padding: 24px;
`
