import React, { createContext, useEffect, useState } from "react";
import { httpClient } from "../infra/http/httpClient";
import { User } from "../models/user.model";

interface UserInfoContextoData {
  usersData: User[];
  setUsersData: any;
  getPaymentByUser: any;
  selectedUserId: any;
}

const UserInfoContexto = createContext<UserInfoContextoData>(
  {} as UserInfoContextoData
);

export const UserInfoProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [usersData, setUsersData] = useState<User[]>([]);
  const [selectedUserId, setSelectedUserId] = useState<any>(-1);

  const getInfoFromApi = async () => {
    const { data } = await httpClient.get("/users");
    if (data) {
      setUsersData(data.users as User[]);
    }
  };

  const getPaymentByUser = async (id: number) => {
    const paymentsData = await (
      await httpClient.get(`/payments?Id=${id}`)
    ).data.payments;
    usersData[id].payments = paymentsData;
    setUsersData(usersData);
    setSelectedUserId(id)
  };

  useEffect(() => {
    getInfoFromApi();
  }, []);

  return (
    <UserInfoContexto.Provider
      value={{ usersData, setUsersData, getPaymentByUser, selectedUserId }}
    >
      {children}
    </UserInfoContexto.Provider>
  );
};

export default UserInfoContexto;
