import { useContext } from "react";
import * as S from "./style";
import UserInfoContexto from "../../context/context";
import Card from "../../components/card/card";

export default function Login() {
  const { usersData } = useContext(UserInfoContexto);

  return (
    <S.Container>
      {usersData &&
        usersData?.map((user, index) => (
          <div key={index}>
            <Card id={user.id} name={user.name} payments={user.payments} />
          </div>
        ))}
    </S.Container>
  );
}
